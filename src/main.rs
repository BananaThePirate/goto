use crossterm::{
    event::{self, Event, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
    ExecutableCommand,
};
use ratatui::{prelude::*, widgets::*};
use rgoto::lib;
use rust_fuzzy_search::{self, fuzzy_search_sorted};
use std::io::{self, stderr};

#[derive(PartialEq)]
enum State {
    Running,
    Goto,
    ConfigMissing,
    Exit,
}

struct App {
    search: String,
    projects: Vec<String>,
    results: Vec<String>,
    selected: usize,
    state: State,
}

fn main() -> io::Result<()> {
    enable_raw_mode()?;
    stderr().execute(EnterAlternateScreen)?;
    let mut terminal = Terminal::new(CrosstermBackend::new(stderr()))?;

    let projects = lib::get_projects();
    let config_missing = projects.is_none();
    let mut app = App {
        search: "".to_string(),
        projects: projects.unwrap_or_default(),
        results: Vec::new(),
        selected: 0,
        state: State::Running,
    };

    if config_missing {
        let mut stop = false;
        app.state = State::ConfigMissing;
        let path = lib::get_config_path();
        let text = vec![
            Line::from(vec![
                "The config file is missing, it has been created at: ".into(),
                path.to_str().unwrap().red(),
            ]),
            Line::from(vec![
                "Press ".into(),
                "<TAB> ".red(),
                "to open the config file, ".into(),
                "Press ".into(),
                "<Enter> ".red(),
                "to close goto".into(),
            ]),
        ];
        let _ = terminal.draw(|f| {
            f.render_widget(
                Paragraph::new(text)
                    .block(
                        Block::new()
                            .title(" Config Missing!!! ".red().bold())
                            .borders(Borders::ALL),
                    )
                    //.style(Style::new().white().on_black())
                    .alignment(Alignment::Center),
                f.size(),
            );
        });
        while !stop {
            if event::poll(std::time::Duration::from_millis(50)).unwrap() {
                if let Event::Key(key) = event::read().unwrap() {
                    if key.kind == event::KeyEventKind::Press {
                        match key.code {
                            KeyCode::Enter => {
                                stop = true;
                            }
                            KeyCode::Tab => {
                                let _ = open::that_detached(lib::get_config_path());
                            }
                            _ => {}
                        }
                    }
                }
            }
        }
    }

    while app.state == State::Running {
        app = handle_events(app);

        //app.results = app.projects.iter().filter(|f| matcher.fuzzy_match(&app.search, f).is_some()).collect();
        let search_input: Vec<&str> = app.projects.iter().map(|x| x.as_ref()).collect();
        let results = fuzzy_search_sorted(&app.search, &search_input);
        let mut app_results = Vec::new();
        for i in results {
            app_results.push(i.0.to_string());
        }
        app.results = app_results;

        terminal.draw(|f| ui(f, &app))?;
    }

    disable_raw_mode()?;
    stderr().execute(LeaveAlternateScreen)?;
    if app.state == State::Goto {
        let output = &app.results[app.selected];
        println!("{}", output);
    } else {
        println!(".");
    }
    Ok(())
}

fn handle_events(mut app: App) -> App {
    if event::poll(std::time::Duration::from_millis(50)).unwrap() {
        if let Event::Key(key) = event::read().unwrap() {
            if key.kind == event::KeyEventKind::Press {
                match key.code {
                    KeyCode::Char(n) => {
                        app.search.push(n);
                    }
                    KeyCode::Backspace => {
                        app.search.pop();
                    }
                    KeyCode::Down => {
                        if app.selected < app.projects.len() - 1 {
                            app.selected += 1;
                        }
                    }
                    KeyCode::Up => {
                        if app.selected >= 1 {
                            app.selected -= 1;
                        }
                    }
                    KeyCode::Enter => {
                        // exit and print output
                        app.state = State::Goto;
                    }
                    KeyCode::Esc => {
                        // cancel
                        app.state = State::Exit;
                    }
                    KeyCode::Tab => {
                        let _ = open::that_detached(lib::get_config_path());
                    }
                    _ => {}
                }
            }
        }
    }
    app
}

fn ui(frame: &mut Frame, app: &App) {
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(vec![
            Constraint::Max(3),
            Constraint::Percentage(50),
            Constraint::Max(3),
        ])
        .split(frame.size());
    // search bar
    frame.render_widget(
        Paragraph::new(app.search.clone()).block(
            Block::default()
                .title(" Search ".bold())
                .borders(Borders::ALL),
        ),
        layout[0],
    );

    // results
    let results: Vec<ListItem<'_>> = app.results.clone().into_iter().map(ListItem::new).collect();
    frame.render_stateful_widget(
        List::new(results)
            .block(
                Block::default()
                    .title(" GOTO ".bold())
                    .borders(Borders::ALL),
            )
            //.style(Style::default().fg(Color::White))
            .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
            .highlight_symbol(">>"),
        layout[1],
        &mut ListState::default().with_selected(Some(app.selected)),
    );

    // help guide
    let text = vec![Line::from(vec![
        "ESC ".red(),
        "cancel, ".into(),
        "<Enter> ".red(),
        "goto selected, ".into(),
        "↑/↓ ".red(),
        "select up/down, ".into(),
        "<TAB> ".red(),
        "Open conifg file".into(),
    ])];
    frame.render_widget(
        Paragraph::new(text)
            .block(Block::new().title(" Help ".bold()).borders(Borders::ALL))
            //.style(Style::new().white().on_black())
            .alignment(Alignment::Center),
        layout[2],
    );

    // let mut scrollbar_state = ScrollbarState::default()
    //     .content_length(EMAILS.len())
    //     .position(selected_index);
    // Scrollbar::default()
    //     .begin_symbol(None)
    //     .end_symbol(None)
    //     .track_symbol(None)
    //     .thumb_symbol("▐")
    //     .render(area[1], buf, &mut scrollbar_state);
}
