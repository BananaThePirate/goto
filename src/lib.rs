pub mod lib {
    use dirs;
    use std::fs;
    use std::io::{BufRead, BufReader};
    pub fn get_projects() -> Option<Vec<String>> {
        let mut config_dir = dirs::config_dir().unwrap();
        config_dir.push("goto/");
        if !config_dir.exists() {
            let _ = fs::create_dir_all(&config_dir);
        }
        config_dir.push("config.txt");
        if !config_dir.exists() {
            let _ = fs::File::create(config_dir);
            return None;
        }
        let file = fs::File::open(config_dir).unwrap();
        let output: Vec<String> = BufReader::new(file).lines().map(|a| a.unwrap()).collect();
        Some(output)
    }
    pub fn get_config_path() -> std::path::PathBuf {
        let mut config_dir = dirs::config_dir().unwrap();
        config_dir.push("goto/");
        config_dir.push("config.txt");
        config_dir
    }
}
