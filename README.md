# RGOTO

fuzzy find through a list of directories and easily `cd` into it

![screenshot](./screenshot.png)


## configuration

No-nonsense, just a damn list of directories

| Platform | Path |
|:-:|:-:|
|Linux|~/.config/goto/config.txt|
|Windows|%APPDATA%\goto\config.txt|

## Install guide

With cargo:

`cargo install --git https://gitlab.com/BananaThePirate/goto.git`


## Usage Guide

Because of how operating systems work, this cannot cd for you, so you need to define a function or alias in your shell

for example:

*Fish*
```
alias goto='cd (rgoto)'
```

## TODO:

 - [ ] ERROR HANDELING
 - [x] make it easier to edit the configuration file
